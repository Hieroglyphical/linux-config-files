"vimrc

so ~/.vim/plugins.vim                                    "Source plugin file
so ~/.vim/cocSettings.vim

set nocompatible                        				         "The latest Vim settings/options 
set backspace=indent,eol,start                           "Make Backspace behave like normal
set showmode                                             "Shows what mode youre editing in
set nowrap                                               "Removes text wrapping
set ignorecase                                           "Ignores case when searching
set smartcase                                            "Will look at case when capital letters are used
set noswapfile                                           "Prevents swap files from being created
set tabstop=4                                            "Number of spaces a tab will create
set shiftwidth=4                                         "Number of spaces to use for autotab
set softtabstop=4
set smarttab
set mouse-=a                                             " Enable the use of the mouse.
set clipboard=unnamedplus                                "Allows coping to system clipboard
set so=5                                     "  this will keep the screen # away from the cursor while scrolling
set numberwidth=1                            "sets the width of the line number column

"----------Visuals---------"

":source ~/.vim/colors/atom-dark-256.vim
colorscheme atom-dark-256


"----------Mappings---------"                            "nmap = commands that happen in normal mode
"shows git status while in a vim file
map <F2> :! git status<cr>

"Change every back slash into a forward slash using \/     
nnoremap <silent> <Leader>/ :let tmp=@/<Bar>s:\\:/:ge<Bar>let @/=tmp<Bar>noh<CR>
"Change every forward slash into a back slash using \\
nnoremap <silent> <Leader><Bslash> :let tmp=@/<Bar>s:/:\\:ge<Bar>let @/=tmp<Bar>noh<CR>

"Automatically source Vimrc file on save
augroup autosourcing
	autocmd!
	autocmd BufWritePost .vimrc source %
augroup END

"Map Nerd Tree to ctrl+1
nmap ,1 :NERDTreeToggle<CR>

"Quick way to search for functions using plugin ctrlP
nmap <c-R> :CtrlPBufTag<CR>


"search for the word the cursor is currently on using ,s
:nnoremap ,s :grep <C-R><C-W> *<CR>

"comment out highlighted lines (using #)
map ,c :s/^/#<CR>  ,<space>

"Toggle line numbers on
map <F3> :set nu!<CR>

"Set the color of line numbers
:highlight LineNr term=bold cterm=NONE ctermfg=Green ctermbg=NONE gui=NONE guifg=Blue guibg=NONE

"Sets up shortcut for clipboard copy
map ,y "+y

"----------Search----------------"                         
"adds a highlight to search
set hlsearch
"add incremental highlighting
set incsearch
"shortcut to remove highlighting
nmap ,<space> :nohlsearch<cr>



"----------Split Mgmt----------------"                         
set splitbelow
set splitright

nmap <C-J> <C-W><C-J>
nmap <C-K> <C-W><C-K>
nmap <C-H> <C-W><C-H>
nmap <C-L> <C-W><C-L>


"----------Abbreviations---------"                         

:abbr hte the
:abbr teh the
:abbr recieve receive


"----------PSR2 Fixes on save---------"                         
autocmd BufWritePost *.php silent! call PhpCsFixerFixFile()




